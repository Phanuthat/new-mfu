import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyDDwAR8CMfViXpiYUOo02Ve1dvBOKfuGKk",
    authDomain: "mfu-exchange-knowledge-4f90b.firebaseapp.com",
    databaseURL: "https://mfu-exchange-knowledge-4f90b.firebaseio.com",
    projectId: "mfu-exchange-knowledge-4f90b",
    storageBucket: "mfu-exchange-knowledge-4f90b.appspot.com",
    messagingSenderId: "598375760087",
    appId: "1:598375760087:web:c1a62036a31f0f31"
  };
firebase.initializeApp(firebaseConfig);

const database = firebase.database();
const auth = firebase.auth();
const provider = new firebase.auth.FacebookAuthProvider();

export { database, auth, provider }
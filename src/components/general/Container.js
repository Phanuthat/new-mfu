import React, { Component, Children } from 'react';
import { Carousel, Button } from 'antd';
import Header from './Header';
import MenuBar from './MenuBar';
class Container extends Component {

    render() {
        const { check, children } = this.props
        return (
            <div style={{ flex: 1 }}>
                {check == 'header' ? <Header /> : <MenuBar />}
                <div>{children}</div>
            </div>
        )
    }

}

export default Container;
import React, { Component } from 'react';
import { List, Card, Typography, Avatar, Button, Icon, Modal, Input, message } from 'antd';
import Header from '../components/general/HeaderMain';
import { Element, animateScroll as scroll } from "react-scroll";
import axios from 'axios'
const { Title } = Typography;
class ManageKnowledgePage extends Component {
    state = {
        typeBlogs: [],
        change: "",
        isClickModal: false,
        titleModal: "",
        blogTypeId: '',
        user: []
    }

    OnModalCancel = () => {
        this.setState({ isClickModal: false });
    };

    OnModalOk = () => {
        this.updateKnowledge(this.state.blogTypeId)
        this.setState({ isClickModal: false });
        this.state.typeBlogs.filter(item => {
            if (item.id === this.state.blogTypeId) {
                return item.typeName = this.state.change
            }
        })
        if (this.state.titleModal === 'add') {
            this.addKnowledge()
        }
    };

    onClickEditKnowledge = (blogTypeId, typeName) => {
        this.setState({ titleModal: "edit" })
        this.setState({ isClickModal: true, change: typeName, blogTypeId: blogTypeId });
    }

    onClickAddKnowledge = () => {
        this.setState({ titleModal: "add" })
        this.setState({ isClickModal: true });
    }

    addKnowledge = () => {
        axios({
            url: `http://localhost:8080/add/blogtype?adminId=${this.state.user.id}`,
            method: 'post',
            data: {
                typeName: this.state.change
            }
        }).then(res => {
            message.success('เพิ่ม knowledge type สำเร็จ', 1);
            this.getTypeBlogs()
        }).catch(e => {
            message.error('เพิ่ม knowledge type ไม่สำเร็จ', 1);
        })
    }

    updateKnowledge = (blogTypeId) => {
        if (blogTypeId) {
            const adminId = 1
            axios.put(`http://localhost:8080/edit/blogtype/${adminId}/${blogTypeId}`,
                {
                    typeName: this.state.change,
                }).catch((error) => {
                    console.log(error)
                })
        } else {
            console.log("blog id not found")
        }

    }
    onKnowledgeChange = (event) => {
        const change = event.target.value
        this.setState({ change })
    }

    componentWillMount() {
        const jsonStr = localStorage.getItem('user-data');
        const user = jsonStr && JSON.parse(jsonStr)
        if (user.role === 'member') {
            this.props.history.push("/post")
        }
        this.setState({ user: user })
        this.getTypeBlogs()
    }

    getTypeBlogs = () => {
        axios.get("http://localhost:8080/blogtype").then(res => {
            if (res.data) {
                let data = res.data
                this.setState({ typeBlogs: data })
            } else {
                console.log("data not found")
            }
        })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <div>
                <Header />
                <div style={{ marginTop: 50, marginBottom: 50, marginLeft: 80, marginRight: 80 }}>
                    <div style={{ flex: 1, justifyContent: 'flex-start', display: 'flex', paddingLeft: 100 }}>
                        <Title level={1}>Manage Knowledge</Title>
                    </div>
                    <div style={{ borderStyle: "solid", borderWidth: "1px", borderColor: "black", margin: 80, borderRadius: 50 }}>
                        <div style={{ marginLeft: 150, marginRight: 150, marginTop: 50, flexDirection: 'row', display: 'flex' }}>
                            <div style={{ height: 84, alignItems: 'center', display: 'flex' }}>
                                <h1 style={{ fontSize: 36 }}>Knowledge</h1>
                            </div>
                            <div style={{ width: '100%', justifyContent: 'flex-end', display: 'flex' }}>
                                <Button
                                    onClick={() => { this.onClickAddKnowledge() }}
                                    type="primary"
                                    size="large"
                                    style={{ margin: 20, backgroundColor: '#00cc66', borderColor: '#00cc66', borderRadius: 200 }}
                                >
                                    <Icon type="plus" /> เพิ่ม
                                </Button>
                            </div>
                        </div>
                        <Element style={{ position: "relative", height: "800px", overflow: "scroll", marginBottom: "100px", margin: "50px" }}>
                            <List
                                itemLayout="horizontal"
                                dataSource={this.state.typeBlogs}
                                renderItem={item => (
                                    <div style={{ paddingTop: "50px", marginLeft: 100, marginRight: 100, justifyContent: 'left', display: 'flex' }}>
                                        <Card
                                            style={{ width: "100%", borderStyle: "solid", borderWidth: "1px", borderColor: "black", borderRadius: 50, marginBottom: 50 }}

                                        >
                                            <List.Item>
                                                <List.Item.Meta
                                                    description={
                                                        <div style={{ flexDirection: 'row', display: 'flex' }}>
                                                            <div style={{ marginLeft: 80, width: '100%', alignItems: 'center', display: 'flex' }}><h1>{item.typeName}</h1></div>
                                                            <div style={{ width: '100%', justifyContent: 'flex-end', display: 'flex' }}>
                                                                <Button
                                                                    onClick={() => { this.onClickEditKnowledge(item.id, item.typeName) }}
                                                                    type="primary"
                                                                    size="large"
                                                                    style={{ margin: 20, backgroundColor: '#2FEB3C', borderColor: '#2FEB3C', borderRadius: 50, height: 44, width: 90 }}
                                                                >
                                                                    <Icon type="edit" /> แก้ไข
                                                                    </Button>
                                                            </div>
                                                        </div>
                                                    }
                                                />
                                            </List.Item>
                                        </Card>
                                    </div>
                                )}
                            />
                        </Element>

                    </div>
                    <div>
                        <Modal
                            title={`${
                                this.state.titleModal === "add" ? "เพิ่ม Knowledge" : "แก้ไข Knowledge"
                                }`}
                            visible={this.state.isClickModal}
                            onOk={() => { this.OnModalOk(); }}
                            onCancel={() => { this.OnModalCancel(); }}
                        >
                            <Input
                                placeholder={`${
                                    this.state.titleModal === "add" ? "เพิ่ม Knowledge" : "แก้ไข Knowledge"
                                    }`}
                                style={{ borderRadius: 5 }}
                                size="large"
                                value={this.state.change}
                                onChange={this.onKnowledgeChange}
                            />

                        </Modal>
                    </div>
                </div>
            </div>
        )
    }

}

export default ManageKnowledgePage;

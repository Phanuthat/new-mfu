import React, { Component } from 'react';
import { List, Card, Typography, message, Button, Icon, Modal, Input } from 'antd';
import Header from '../components/general/HeaderMain';
import { Element, animateScroll as scroll } from "react-scroll";
import axios from 'axios';
const { Title } = Typography;
class ManageAdminPage extends Component {

    state = {
        user: [],
        adminUserList: [],
        userList: [],
        isClickModal: false,
    }

    componentWillMount() {
        const jsonStr = localStorage.getItem('user-data');
        const user = jsonStr && JSON.parse(jsonStr)
        this.setState({ user: user })
        if (user.role === 'member') {
            this.props.history.push("/post")
        }
        this.setState({ user: user })
        this.getUserList()
        axios({
            url: `http://localhost:8080/user/admin?role=${user.role}`,
            method: 'get'
        }).then(res => {
            this.setState({ adminUserList: res.data })
        })
    }

    onClickDeleteButton = (userId) => {
        axios({
            url: `http://localhost:8080/delete/admin?adminId=${this.state.user.id}&userId=${userId}`,
            method: 'put'
        }).then(res => {
            this.getAdminUserList()
            message.success('ลบ Admin สำเร็จ', 1);
        }).catch(e => {
            message.error('ลบ Admin ไม่สำเร็จ', 1);
        })
    }

    getAdminUserList = () => {
        axios({
            url: `http://localhost:8080/user/admin?role=${this.state.user.role}`,
            method: 'get'
        }).then(res => {
            this.setState({ adminUserList: res.data })
        })
    }

    getUserList = () => {
        axios({
            url: 'http://localhost:8080/getalluser',
            method: 'get'
        }).then(res => {
            this.setState({ userList: res.data })
        }).catch(e => {
            console.log("error");
        })
    }

    OnModalCancel = () => {
        this.setState({ isClickModal: false });
    };

    onClickAddAdminButton = (userId) => {
        axios({
            url: `http://localhost:8080/add/admin?adminId=${this.state.user.id}&userId=${userId}`,
            method: 'put'
        }).then(res => {
            this.getAdminUserList()
            message.success('เพิ่ม Admin สำเร็จ', 1);
        }).catch(e => {
            message.error('เพิ่ม Admin ไม่สำเร็จ', 1);
        })
    }

    render() {
        return (
            <div>
                <Header />
                <div style={{ marginTop: 50, marginBottom: 50, marginLeft: 80, marginRight: 80 }}>
                    <div style={{ flex: 1, justifyContent: 'flex-start', display: 'flex', paddingLeft: 100 }}>
                        <Title level={1}>Manage Admin</Title>
                    </div>
                    <div style={{ borderStyle: "solid", borderWidth: "1px", borderColor: "black", margin: 80, borderRadius: 50 }}>
                        <div style={{ marginLeft: 150, marginRight: 150, marginTop: 50, flexDirection: 'row', display: 'flex' }}>
                            <div style={{ height: 84, alignItems: 'center', display: 'flex' }}>
                                <h1 style={{ fontSize: 36 }}>Admin</h1>
                            </div>
                            <div style={{ width: '100%', justifyContent: 'flex-end', display: 'flex' }}>
                                <Button
                                    type="primary"
                                    size="large"
                                    style={{ margin: 20, backgroundColor: '#00cc66', borderColor: '#00cc66', borderRadius: 200 }}
                                    onClick={() => { this.setState({ isClickModal: true }) }}
                                >
                                    <Icon type="plus" /> เพิ่ม
                                </Button>
                            </div>
                        </div>
                        <Element style={{ position: "relative", height: "800px", overflow: "scroll", marginBottom: "100px", margin: "50px" }}>
                            <List
                                itemLayout="horizontal"
                                dataSource={this.state.adminUserList}
                                renderItem={item => (
                                    <div style={{ paddingTop: "50px", marginLeft: 100, marginRight: 100, justifyContent: 'left', display: 'flex' }}>
                                        <Card
                                            style={{ width: "100%", borderStyle: "solid", borderWidth: "1px", borderColor: "black", borderRadius: 50, marginBottom: 50 }}
                                        >
                                            <List.Item>
                                                <List.Item.Meta
                                                    description={
                                                        <div style={{ flexDirection: 'row', display: 'flex' }}>
                                                            <div style={{ marginLeft: 80, width: '100%', alignItems: 'center', display: 'flex' }}><h1>{item.name}</h1></div>
                                                            <div style={{ width: '100%', justifyContent: 'flex-end', display: 'flex' }}>
                                                                <Button
                                                                    type="primary"
                                                                    size="large"
                                                                    style={{ margin: 20, backgroundColor: '#ff0000', borderColor: '#ff0000', borderRadius: 50, height: 44, width: 90 }}
                                                                    onClick={() => this.onClickDeleteButton(item.id)}
                                                                >
                                                                    <Icon type="delete" /> ลบ
                                                                    </Button>
                                                            </div>
                                                        </div>
                                                    }
                                                />
                                            </List.Item>
                                        </Card>
                                    </div>
                                )}
                            />
                        </Element>

                    </div>
                </div>
                <Modal
                    title="เพิ่ม Admin"
                    visible={this.state.isClickModal}
                    onOk={() => { this.OnModalCancel(); }}
                    onCancel={() => { this.OnModalCancel(); }}
                    width="1200px"
                >
                    <Element style={{ position: "relative", height: "800px", overflow: "scroll", marginBottom: "100px", margin: "50px" }}>
                        <List
                            itemLayout="horizontal"
                            dataSource={this.state.userList}
                            renderItem={item => (
                                <div style={{ paddingTop: "50px", marginLeft: 100, marginRight: 100, justifyContent: 'left', display: 'flex' }}>
                                    <Card
                                        style={{ width: "100%", borderStyle: "solid", borderWidth: "1px", borderColor: "black", borderRadius: 50, marginBottom: 50 }}
                                    >
                                        <List.Item>
                                            <List.Item.Meta
                                                description={
                                                    <div style={{ flexDirection: 'row', display: 'flex' }}>
                                                        <div style={{ marginLeft: 80, width: '100%', alignItems: 'center', display: 'flex' }}><h1>{item.name}</h1></div>
                                                        <div style={{ width: '100%', justifyContent: 'flex-end', display: 'flex' }}>
                                                            <Button
                                                                type="primary"
                                                                size="large"
                                                                style={{ margin: 20, backgroundColor: '#00cc66', borderColor: '#00cc66', borderRadius: 50, height: 44, width: 90 }}
                                                                onClick={() => this.onClickAddAdminButton(item.id)}
                                                            >
                                                                <Icon type="plus" /> เพิ่ม
                                                                    </Button>
                                                        </div>
                                                    </div>
                                                }
                                            />
                                        </List.Item>
                                    </Card>
                                </div>
                            )}
                        />
                    </Element>
                </Modal>
            </div>
        )
    }

}

export default ManageAdminPage;